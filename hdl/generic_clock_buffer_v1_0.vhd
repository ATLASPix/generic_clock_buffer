library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
Library UNISIM;
use UNISIM.vcomponents.all;

entity generic_clock_buffer_v1_0 is

	port (
		-- Users to add ports here
        clk_in_1 : in std_logic;
        clk_out_1_p : out std_logic;
        clk_out_1_n : out std_logic;
        clk_in_2 : in std_logic;
        clk_out_2_p : out std_logic;
        clk_out_2_n : out std_logic      
		-- User ports ends

	);
end generic_clock_buffer_v1_0;

architecture arch_imp of generic_clock_buffer_v1_0 is


begin


	-- Add user logic here
   OBUFDS_inst1 : OBUFDS
    generic map (
       IOSTANDARD => "DEFAULT", -- Specify the output I/O standard
       SLEW => "SLOW")          -- Specify the output slew rate
    port map (
       O => clk_out_1_p,     -- Diff_p output (connect directly to top-level port)
       OB => clk_out_1_n,   -- Diff_n output (connect directly to top-level port)
       I => clk_in_1      -- Buffer input 
    );   
    
    OBUFDS_inst2 : OBUFDS
       generic map (
          IOSTANDARD => "DEFAULT", -- Specify the output I/O standard
          SLEW => "SLOW")          -- Specify the output slew rate
       port map (
          O => clk_out_2_p,     -- Diff_p output (connect directly to top-level port)
          OB => clk_out_2_n,   -- Diff_n output (connect directly to top-level port)
          I => clk_in_2     -- Buffer input 
       );
	-- User logic ends

end arch_imp;
